<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (C) 2015 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->
<!-- Volume section defines a volume curve for a given use case and device category.
It contains a list of points of this curve expressing the attenuation in Millibels for a given
volume index from 0 to 100.
<volume deviceCategory=””>
<point>0,-9600</point>
<point>100,0</point>
</volume>
-->

<volumeGroups>
    <volumeGroup>
        <name>voice_call</name>
        <indexMin>1</indexMin>
        <indexMax>5</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET">
              <point>0,-4200</point>
              <point>33,-2800</point>
              <point>66,-1400</point>
              <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB">
              <point>0,-4200</point>
              <point>33,-2800</point>
              <point>66,-1400</point>
              <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP">
              <point>0,-4200</point>
              <point>33,-2800</point>
              <point>66,-1400</point>
              <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER">
            <point>0,-2400</point>
            <point>33,-1600</point>
            <point>66,-800</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE">
            <point>0,-2400</point>
            <point>33,-1600</point>
            <point>66,-800</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID">
              <point>0,-4200</point>
              <point>33,-2800</point>
              <point>66,-1400</point>
              <point>100,0</point>
        </volume>
    </volumeGroup>

    <volumeGroup>
        <name>system</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET">
             <point>1,-3000</point>
             <point>33,-2600</point>
             <point>66,-2200</point>
             <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB">
             <point>1,-3000</point>
             <point>33,-2600</point>
             <point>66,-2200</point>
             <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP">
             <point>1,-3000</point>
             <point>33,-2600</point>
             <point>66,-2200</point>
             <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET_CE">
            <point>1,-7450</point>
            <point>13,-5700</point>
            <point>20,-5200</point>
            <point>27,-4700</point>
            <point>33,-4450</point>
            <point>40,-4200</point>
            <point>47,-4000</point>
            <point>53,-3600</point>
            <point>60,-3200</point>
            <point>66,-2800</point>
            <point>73,-2310</point>
            <point>80,-1960</point>
            <point>87,-1780</point>
            <point>93,-1610</point>
            <point>100,-1250</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB_CE">
            <point>1,-7200</point>
            <point>33,-4600</point>
            <point>73,-1500</point>
            <point>100,-1100</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP_CE">
             <point>1,-6600</point>
            <point>13,-6100</point>
            <point>20,-5650</point>
            <point>27,-5150</point>
            <point>33,-4600</point>
            <point>40,-4200</point>
            <point>47,-3800</point>
            <point>53,-3400</point>
            <point>60,-3050</point>
            <point>66,-2600</point>
            <point>73,-2450</point>
            <point>80,-2150</point>
            <point>87,-1800</point>
            <point>93,-1500</point>
            <point>100,-1100</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER" ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID">
             <point>1,-3000</point>
             <point>33,-2000</point>
             <point>66,-1000</point>
             <point>100,0</point>
        </volume>
    </volumeGroup>

    <volumeGroup>
        <name>ring</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_USB" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER">
            <point>1,-2970</point>
            <point>33,-2010</point>
            <point>66,-1020</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_DEVICE_CATEGORY_EARPIECE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>music</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET_CE">
            <point>1,-7450</point>
            <point>13,-5700</point>
            <point>20,-5200</point>
            <point>27,-4700</point>
            <point>33,-4450</point>
            <point>40,-4200</point>
            <point>47,-4000</point>
            <point>53,-3600</point>
            <point>60,-3200</point>
            <point>66,-2800</point>
            <point>73,-2310</point>
            <point>80,-1960</point>
            <point>87,-1780</point>
            <point>93,-1610</point>
            <point>100,-1250</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB_CE">
            <point>1,-7200</point>
            <point>33,-4600</point>
            <point>73,-1500</point>
            <point>100,-1100</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP_CE">
             <point>1,-6600</point>
            <point>13,-6100</point>
            <point>20,-5650</point>
            <point>27,-5150</point>
            <point>33,-4600</point>
            <point>40,-4200</point>
            <point>47,-3800</point>
            <point>53,-3400</point>
            <point>60,-3050</point>
            <point>66,-2600</point>
            <point>73,-2450</point>
            <point>80,-2150</point>
            <point>87,-1800</point>
            <point>93,-1500</point>
            <point>100,-1100</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER">
            <point>0,-75800</point>
            <point>1,-7317</point>
            <point>2,-6450</point>
            <point>7,-5730</point>
            <point>13,-5097</point>
            <point>20,-4216</point>
            <point>27,-3718</point>
            <point>33,-3270</point>
            <point>40,-2976</point>
            <point>47,-2625</point>
            <point>53,-2479</point>
            <point>60,-2229</point>
            <point>66,-2092</point>
            <point>73,-1794</point>
            <point>80,-1620</point>
            <point>87,-1480</point>
            <point>93,-1240</point>
            <point>100,-1180</point>
            <point>109,-960</point>
            <point>116,-850</point>
            <point>122,-640</point>
            <point>130,-500</point>
            <point>137,-350</point>
            <point>143,-200</point>
            <point>150,-30</point>  
        </volume>
         <volume deviceCategory="DEVICE_CATEGORY_HEADSET">
            <point>0,-75800</point>
            <point>1,-6450</point>
            <point>2,-6400</point>
            <point>7,-6209</point>
            <point>13,-5890</point>
            <point>20,-5490</point>
            <point>27,-5118</point>
            <point>33,-4804</point>
            <point>40,-4472</point>
            <point>47,-4096</point>
            <point>53,-3835</point>
            <point>60,-3550</point>
            <point>66,-3292</point>
            <point>73,-3041</point>
            <point>80,-2750</point>
            <point>87,-2457</point>
            <point>93,-2203</point>
            <point>100,-1929</point>
            <point>109,-1638</point>
            <point>116,-1395</point>
            <point>122,-1191</point>
            <point>130,-898</point>
            <point>137,-626</point>
            <point>143,-390</point>
            <point>150,-150</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB">
            <point>0,-75800</point>
            <point>1,-6500</point>
            <point>2,-6416</point>
            <point>7,-6250</point>
            <point>13,-5916</point>
            <point>20,-5500</point>
            <point>27,-5000</point>
            <point>33,-4657</point>
            <point>40,-4371</point>
            <point>47,-3966</point>
            <point>53,-3685</point>
            <point>60,-3400</point>
            <point>66,-3171</point>
            <point>73,-2933</point>
            <point>80,-2600</point>
            <point>87,-2314</point>
            <point>93,-2066</point>
            <point>100,-1800</point>
            <point>109,-1457</point>
            <point>116,-1200</point>
            <point>123,-950</point>
            <point>130,-750</point>
            <point>137,-466</point>
            <point>143,-250</point>
            <point>150,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP">
            <point>0,-75800</point>
            <point>1,-4900</point>
            <point>2,-4825</point>
            <point>7,-4675</point>
            <point>13,-4375</point>
            <point>20,-4000</point>
            <point>27,-3642</point>
            <point>33,-3385</point>
            <point>40,-3157</point>
            <point>47,-2833</point>
            <point>53,-2585</point>
            <point>60,-2300</point>
            <point>66,-2128</point>
            <point>73,-1950</point>
            <point>80,-1700</point>
            <point>87,-1485</point>
            <point>93,-1300</point>
            <point>100,-1100</point>
            <point>109,-885</point>
            <point>116,-707</point>
            <point>122,-571</point>
            <point>130,-428</point>
            <point>137,-266</point>
            <point>143,-142</point>
            <point>150,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID"  ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>assistant</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_USB" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER" ref="DEFAULT_DEVICE_CATEGORY_SPEAKER_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID"  ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>alarm</name>
        <indexMin>1</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET" ref="DEFAULT_NON_MUTABLE_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_USB" ref="DEFAULT_NON_MUTABLE_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP" ref="DEFAULT_NON_MUTABLE_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER">
            <point>0,-2970</point>
            <point>33,-2010</point>
            <point>66,-1020</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_NON_MUTABLE_EARPIECE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_NON_MUTABLE_EXT_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="DEFAULT_NON_MUTABLE_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>notification</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_USB" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER">
            <point>1,-2970</point>
            <point>33,-2010</point>
            <point>66,-1020</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_DEVICE_CATEGORY_EARPIECE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>bluetooth_sco</name>
        <indexMin>1</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET">
            <point>0,-420</point>
            <point>33,-280</point>
            <point>66,-140</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB">
            <point>0,-420</point>
            <point>33,-280</point>
            <point>66,-140</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP">
            <point>0,-420</point>
            <point>33,-280</point>
            <point>66,-140</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER">
            <point>0,-2400</point>
            <point>33,-1600</point>
            <point>66,-800</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE">
            <point>0,-4200</point>
            <point>33,-2800</point>
            <point>66,-1400</point>
            <point>100,0</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID">
            <point>0,-420</point>
            <point>33,-280</point>
            <point>66,-140</point>
            <point>100,0</point>
        </volume>
    </volumeGroup>

    <volumeGroup>
        <name>enforced_audible</name>
        <indexMin>0</indexMin>
        <indexMax>7</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET">
            <point>1,-3000</point>
            <point>33,-2600</point>
            <point>66,-2200</point>
            <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB">
            <point>1,-3000</point>
            <point>33,-2600</point>
            <point>66,-2200</point>
            <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP">
            <point>1,-3000</point>
            <point>33,-2600</point>
            <point>66,-2200</point>
            <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER" ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>dtmf</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET">
            <point>1,-3000</point>
            <point>33,-2600</point>
            <point>66,-2200</point>
            <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_USB">
            <point>1,-3000</point>
            <point>33,-2600</point>
            <point>66,-2200</point>
            <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP">
            <point>1,-3000</point>
            <point>33,-2600</point>
            <point>66,-2200</point>
            <point>100,-1800</point>
        </volume>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER" ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>tts</name>
        <indexMin>0</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET" ref="SILENT_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_USB" ref="SILENT_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP" ref="SILENT_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER" ref="FULL_SCALE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="SILENT_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="SILENT_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="SILENT_VOLUME_CURVE"/>
    </volumeGroup>

    <volumeGroup>
        <name>accessibility</name>
        <indexMin>1</indexMin>
        <indexMax>15</indexMax>
        <volume deviceCategory="DEVICE_CATEGORY_HEADSET" ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_USB" ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_A2DP" ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_SPEAKER" ref="DEFAULT_NON_MUTABLE_SPEAKER_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EARPIECE" ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_EXT_MEDIA" ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
        <volume deviceCategory="DEVICE_CATEGORY_HEARING_AID" ref="DEFAULT_NON_MUTABLE_HEARING_AID_VOLUME_CURVE"/>
    </volumeGroup>

</volumeGroups>

